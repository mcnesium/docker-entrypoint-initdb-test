DROP DATABASE IF EXISTS lbsn;

CREATE DATABASE testdb WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    TEMPLATE = template0
    CONNECTION LIMIT = -1;

\c testdb

CREATE TABLE "test" (
    origin_id serial PRIMARY KEY,
    name text UNIQUE
);
