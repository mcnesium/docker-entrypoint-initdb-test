#!/usr/bin/env bash

test -x "$(command -v ssh)" || apt-get update -y && apt-get install -y openssh-client

mkdir -p ~/.ssh
eval $(ssh-agent -s)
test -f /.dockerenv && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
ssh-add <(echo "$GITLAB_CI_PRIVKEY")

test -x "$(command -v rsync)" || apt-get update -y && apt-get install -y rsync

rsync -rav --chmod=D755,F644 -e "ssh -l gitlab-ci" --exclude=".git" --delete ./ "gitlab-ci@$DEPLOY_SERVER:test/"

ssh "gitlab-ci@$DEPLOY_SERVER" "cd test && docker-compose down && docker-compose up --build --detach"
